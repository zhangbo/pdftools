#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Bo Zhang

import os

class translator:
    """
        translator class:
        members:
            self.__dictionary: dictionary contains all text in local language;dict of dict 
            self.__language: the language used; str
    """
    def __init__(self):
        self.__dictionary = {}
        self.__language = None

    def __repr__(self):
        """ return information """
        for k, v in self.__dictionary:
            print('lang:', k)
            for x, y in v:
                print('   ', x, '=', y)

    def read_language_file(self, filename):
        """
            read language file to set dictionary
            arguments:
                filename: language file name; str
        """
        try:
            filein = open(filename, 'r', encoding='utf-8')
        except:
            raise IOError('cannot open file: ' + filename)

        for line in filein.readlines():
            if line.strip().startswith('#'):
                # comment
                continue

            if '=' in line:
                items = line.split('=')
                if len(items) == 2:
                    self.__dictionary[self.__language][items[0].strip()] = items[1].strip()
            

    def set_language(self, language, directory='lang'):
        """
            set language to be used
            arguments:
                language: language; str
                directory: directory contains language file; str, defaults to 'lang'
        """
        self.__language = language
        filename = os.path.join(os.path.abspath(os.path.dirname(__file__)), directory, language + '.lang')
        self.__dictionary[self.__language] = {}
        self.read_language_file(filename)

    def translate(self, text):
        """
            translate input text to local text
            arguments:
                text: input text; str
            returns:
                local text: str
        """
        if self.__language in self.__dictionary.keys():
            if text in self.__dictionary[self.__language].keys():
                return self.__dictionary[self.__language][text]
        return ''

