#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Bo Zhang

import argparse
import os
import PyPDF2
import re
import tkinter as tk

from tkinter import ttk
from tkinter.filedialog import askdirectory, askopenfilename, askopenfilenames, asksaveasfilename
from tkinter.messagebox import askokcancel, showerror, showinfo, showwarning

from translator import translator 

i18n = translator()

class application(tk.Frame):
    """
        class Application
        members:
            working_directory: working directory; StringVar
            debug: debug flag: bool
            combine_input: pdf files to be combined; list of str
            combine_listbox: listbox to show all pdf files to be combined; tk.Listbox
            extract_input: pdf file to be splitted; StringVar
            extract_listbox: listbox to show all extracted pdf file names; tk.Listbox
            extract_output_directory: output directory of extract pdf files; StringVar
            extract_output_prefix: output prefix of extracted pdf files; StringVar
            extract_pages: pages of the pdf file to be splitted; StringVar
            extract_pdf_total_pages: total number of pages of the pdf file to be splitted; StringVar
    """
    def __init__(self, master=None, debug=False):
        tk.Frame.__init__(self, master)
        master.title('PDFTools')
        self.working_directory = os.curdir
        self.debug = debug
        
        self.combine_input = [] 
        self.combine_listbox = None

        self.extract_input = tk.StringVar()
        self.extract_listbox = None
        self.extract_output_directory = tk.StringVar()
        self.extract_output_prefix = tk.StringVar()
        self.extract_pages = tk.StringVar()
        self.extract_pdf_total_pages = tk.StringVar()

        self.GUI(master)

    def browse_files(self):
        """ browse and select input pdf files to be combined """
        files = askopenfilenames(defaultextension='.pdf', filetype=[(i18n.translate('browse_files_filetype'), '*.pdf')], initialdir=self.working_directory, title=i18n.translate('browse_files_title')) 
        if len(files) > 0:
            self.working_directory = os.path.split(files[0])[0]
            self.combine_input += files
            self.combine_listbox.delete(0, tk.END)
            for f in self.combine_input:
                self.combine_listbox.insert(tk.END, f) 

    def browse_extract_input(self):
        """ browse and select input pdf file to be splitted """
        f = askopenfilename(defaultextension='.pdf', filetype=[(i18n.translate('browse_extract_input_filetype'), '*.pdf')], initialdir=self.working_directory, initialfile=i18n.translate('browse_extract_input_initialfile'), title=i18n.translate('browse_extract_input_title')) 
        if f != '':
            self.working_directory = os.path.dirname(f)
            self.extract_input.set(f)
            try:
                pdfin = PyPDF2.PdfFileReader(open(self.extract_input.get(), 'rb')) 
                self.extract_pdf_total_pages.set(i18n.translate('browse_extract_input_total_pages').format(pdfin.getNumPages()))
            except IOError:
                showerror(i18n.translate('error'), i18n.translate('cannot_read_file') + ': ' + self.extract_input.get())
                raise

    def browse_extract_output_directory(self):
        """ browse and select output directory for extracted pdf files """
        d = askdirectory(initialdir=self.working_directory, title=i18n.translate('browse_extract_output_directory_title')) 
        if d != '':
            self.extract_output_directory.set(d)

    def combine(self):
        """ combine pdf files """
        if len(self.combine_input) == 0:
            return 

        output_filename = asksaveasfilename(defaultextension='.pdf', filetype=[(i18n.translate('combine_output_filename_filetype'), '*.pdf')], initialdir=self.working_directory, initialfile=i18n.translate('combine_output_filename_initialfile'), title=i18n.translate('combine_output_filename_title'))

        if not output_filename:
            i = 1
            while True:
                output_filename = os.path.join(self.working_directory, i18n.translate('combine_output_filename').format(i))
                if os.path.isfile(output_filename):
                    i += 1
                else:
                    break
            ok = askokcancel(i18n.translate('confirm'), i18n.translate('saveas') + ': ' + output_filename)
            if ok:
                pass
            else:
                return
        #elif os.path.isfile(output_filename):
        #    ok = askokcancel(i18n.translate('confirm'), output_filename + i18n.translate('is_overwritten'))
        #    if ok:
        #        pass
        #    else:
        #        return

        merger = PyPDF2.PdfFileMerger()
        try:
            for f in self.combine_input:
                merger.append(fileobj=open(f, 'rb'))
        except IOError:
            showerror(i18n.translate('error'), i18n.translate('cannot_read_file') + ': ' + f)
            raise
        fileout = open(output_filename, 'wb')
        merger.write(fileout)
        fileout.close()
        showinfo(i18n.translate('done'), i18n.translate('combine_conclusion'))

    def GUI(self, master):
        """
            create the main UI interface
            arguments:
                master: the parent window; tk.Tk()
        """
        nb = ttk.Notebook(master)

        tab_combine = ttk.Frame(nb)
        tab_combine.grid()
        tk.Label(tab_combine, text=i18n.translate('ui_combine_label_input')).grid(row=0, columnspan=3)

        combine_xscrollbar = tk.Scrollbar(tab_combine, orient=tk.HORIZONTAL)
        combine_yscrollbar = tk.Scrollbar(tab_combine, orient=tk.VERTICAL)
        self.combine_listbox = tk.Listbox(tab_combine, xscrollcommand=combine_xscrollbar.set, yscrollcommand=combine_yscrollbar.set, width=80)
        self.combine_listbox.grid(row=1, column=0, rowspan=4)
        combine_xscrollbar.grid(row=5, column=0, sticky=tk.E + tk.W)
        combine_xscrollbar.config(command=self.combine_listbox.xview)
        combine_yscrollbar.grid(row=1, column=1, rowspan=4, sticky=tk.N + tk.S)
        combine_yscrollbar.config(command=self.combine_listbox.yview)
        tk.Button(tab_combine, text=i18n.translate('ui_combine_button_add'), command=self.browse_files, width=5).grid(row=1, column=2) 

        tk.Button(tab_combine, text=i18n.translate('ui_combine_button_move_up'), command=self.move_up, width=5).grid(row=2, column=2)

        tk.Button(tab_combine, text=i18n.translate('ui_combine_button_move_down'), command=self.move_down,width=5).grid(row=3, column=2)

        tk.Button(tab_combine, text=i18n.translate('ui_combine_button_delete'), command=self.delete, width=5).grid(row=4, column=2) 

        tk.Button(tab_combine, text=i18n.translate('ui_combine_button_combine'), command=self.combine, width=10).grid(row=6, columnspan=3) 



        tab_extract = ttk.Frame(nb)
        tab_extract.grid()
        tk.Label(tab_extract, text=i18n.translate('ui_extract_label_input')).grid(row=0, column=0)
        tk.Entry(tab_extract, textvariable=self.extract_input, width=50).grid(row=0, column=1)
        tk.Button(tab_extract, text=i18n.translate('ui_extract_button_browse'), command=self.browse_extract_input, width=8).grid(row=0, column=2) 

        tk.Label(tab_extract, text=i18n.translate('ui_extract_label_description')).grid(row=1, column=0)
        tk.Entry(tab_extract, textvariable=self.extract_pages, width=50).grid(row=1, column=1)
        tk.Entry(tab_extract, textvariable=self.extract_pdf_total_pages, state='readonly', width=10).grid(row=1, column=2)

        tk.Label(tab_extract, text=i18n.translate('ui_extract_label_output_directory')).grid(row=2, column=0)
        tk.Entry(tab_extract, textvariable=self.extract_output_directory, width=50).grid(row=2, column=1)
        tk.Button(tab_extract, text=i18n.translate('ui_extract_button_browse'), command=self.browse_extract_output_directory, width=8).grid(row=2, column=2) 

        tk.Label(tab_extract, text=i18n.translate('ui_extract_label_output_prefix')).grid(row=3, column=0)
        tk.Entry(tab_extract, textvariable=self.extract_output_prefix, width=50).grid(row=3, column=1)

        extract_xscrollbar = tk.Scrollbar(tab_extract, orient=tk.HORIZONTAL)
        extract_yscrollbar = tk.Scrollbar(tab_extract, orient=tk.VERTICAL)
        self.extract_listbox = tk.Listbox(tab_extract, xscrollcommand=extract_xscrollbar.set, yscrollcommand=extract_yscrollbar.set, width=80)
        self.extract_listbox.grid(row=4, column=0, columnspan=2)
        extract_xscrollbar.grid(row=5, column=0, columnspan=2, sticky=tk.E + tk.W)
        extract_xscrollbar.config(command=self.extract_listbox.xview)
        extract_yscrollbar.grid(row=4, column=2, sticky=tk.N + tk.S + tk.W)
        extract_yscrollbar.config(command=self.extract_listbox.yview)

        tk.Button(tab_extract, text=i18n.translate('ui_extract_button_extract'), command=self.extract, width=10).grid(row=6, columnspan=3)

        nb.add(tab_combine, text=i18n.translate('ui_tab_title_combine'), padding=2)
        nb.add(tab_extract, text=i18n.translate('ui_tab_title_extract'), padding=2)
        nb.pack(expand=1, fill="both")

    def delete(self):
        """ delete selected file """
        indexes = self.combine_listbox.curselection()
        if len(indexes) == 0:
            return 

        for i in indexes:
            self.combine_input.pop(i)

        self.combine_listbox.delete(0, tk.END)
        for f in self.combine_input: 
            self.combine_listbox.insert(tk.END, f)

    def extract(self):
        """ extract pdf file """
        try:
            pdfin = PyPDF2.PdfFileReader(open(self.extract_input.get(), 'rb')) 
        except IOError:
            showerror(i18n.translate('error'), i18n.translate('cannot_read_file') + ': ' + self.extract_input.get())
            raise
        
        output_directory = self.extract_output_directory.get()
        output_prefix = self.extract_output_prefix.get()
        output_suffix = re.split(r',|，' , self.extract_pages.get())
        pages = {}
        for s in output_suffix:
            m1 = re.match(r'([0-9]+)$', s.strip())
            m2 = re.match(r'([0-9]+)-([0-9]+)$', s.strip())
            if m1:
                page_start = int(m1.group(1))
                page_end = page_start
                if page_end < pdfin.getNumPages() + 1 and page_start >= 1:
                    pages[s.strip()] = [page_start, page_end]
            if m2:
                if len(m2.group(1)) + len(m2.group(2)) + 1 == len(s.strip()):
                    page_start = int(m2.group(1))
                    page_end = int(m2.group(2))
                    if page_end < pdfin.getNumPages() + 1 and page_start <= page_end and page_start >= 1:
                        pages[s.strip()] = [page_start, page_end]
         
        if len(pages) == 0:
            for i in range(pdfin.getNumPages()):
                pages[str(i + 1)] = [i + 1, i + 1]

        if not output_directory:
            output_directory = self.working_directory

        if not output_prefix:
            output_prefix = i18n.translate('extract_output_prefix') 
            i = 1
            while True:
                success = True
                for s in pages.keys():
                    output_filename = os.path.join(output_directory, output_prefix + '{0:d}_p{1:s}.pdf'.format(i, s))
                    if os.path.isfile(output_filename):
                        i += 1
                        success = False
                        break
                if success:
                    break
            ok = askokcancel(i18n.translate('confirm'), i18n.translate('extract_save_output_prefix_msg') + ': ' + output_prefix)
            if ok:
                pass
            else:
                return
        else:
            for s in pages.keys():
                output_filename = os.path.join(output_directory, output_prefix + '_p{0:s}.pdf'.format(s)) 
                if os.path.isfile(output_filename):
                    ok = askokcancel(i18n.translate('confirm'), output_filename + i18n.translate('is_overwritten'))
                    if ok:
                        pass
                    else:
                        showinfo(i18n.translate('cancel'), i18n.translate('extract_cancel_output_prefix_msg'))
                        return

        self.extract_listbox.delete(0, tk.END)
        for s in pages.keys():
            self.extract_listbox.insert(tk.END, os.path.join(output_directory, output_prefix + '_p{0:s}.pdf'.format(s)))
        
        for k, v in pages.items():
            pdfout = PyPDF2.PdfFileWriter()
            output_filename = os.path.join(output_directory, output_prefix + '_p{0:s}.pdf'.format(k))
            for i in range(v[0] - 1, v[1]):
                pdfout.addPage(pdfin.getPage(i))
            fileout = open(output_filename, 'wb')
            pdfout.write(fileout)
            fileout.close()
        showinfo(i18n.translate('done'), i18n.translate('extract_conclusion'))


    def move_down(self):
        """ move selected file down one entry """
        indexes = self.combine_listbox.curselection()
        if len(indexes) == 0:
            return 

        for i in indexes:
            if i < len(self.combine_input) - 1:
                self.combine_input.insert(i + 1, self.combine_input.pop(i))

        self.combine_listbox.delete(0, tk.END)
        for f in self.combine_input: 
            self.combine_listbox.insert(tk.END, f)

        if indexes[0] == len(self.combine_input) - 1:
            self.combine_listbox.activate(indexes[0])
            self.combine_listbox.selection_set(indexes[0])
        else:
            self.combine_listbox.activate(indexes[0] + 1) 
            self.combine_listbox.selection_set(indexes[0] + 1) 

    def move_up(self):
        """ move selected file up one entry """
        indexes = self.combine_listbox.curselection()
        if self.debug:
            print(indexes)

        if len(indexes) == 0:
            return 

        for i in indexes:
            if i > 0:
                self.combine_input.insert(i - 1, self.combine_input.pop(i))

        self.combine_listbox.delete(0, tk.END)
        for f in self.combine_input:
            self.combine_listbox.insert(tk.END, f)

        if indexes[0] == 0:
            self.combine_listbox.activate(0)
            self.combine_listbox.selection_set(0)
        else:
            self.combine_listbox.activate(indexes[0] - 1) 
            self.combine_listbox.selection_set(indexes[0] - 1) 

def main():
    # parse options
    parser = argparse.ArgumentParser(description='combine and extract pdf files, created 6-14-2016')

    parser.add_argument('--combine', nargs='+', help='combine pdf files')
    parser.add_argument('--combine_all', action='store_true', default=False, help='combine all pdf files')
    parser.add_argument('-d', '--debug', action='store_true', default=False, help='debug')
    parser.add_argument('--extract', help='extract from pdf file')
    parser.add_argument('--ui_language', choices=['en-US', 'zh-CN'], default='zh-CN', help='ui langauge')
    parser.add_argument('--pdfs_directory', default='.', help='combine all pdf files inside this directory')
    parser.add_argument('-o', '--output', help='output pdf file')
    parser.add_argument('-v', '--verbose', action='store_true', default=False, help='print verbose information')

    args = parser.parse_args()
    if args.verbose:
        print(args)

    if args.combine:
        if len(args.combine) >= 2:
            pdf_combine(args.combine, output_filename=args.output, is_verbose=args.verbose)
    elif args.combine_all:
        pdfs = []
        for f in sorted(os.listdir(args.pdfs_directory)):
            if f.endswith('.pdf'):
                pdfs.append(pdf)
        pdf_combine(pdfs, output_filename=args.output, is_verbose=args.verbose)
    elif args.extract:
        pdf_extract(args.extract, output_filename=args.output, is_verbose=args.verbose)
    else:
        i18n.set_language(args.ui_language)
        root = tk.Tk()
        app = application(master=root, debug=args.debug)
        app.mainloop()

def pdf_combine(filenames, output_filename, is_verbose=False):
    """
        combine pdf files
        arguments:
            filenames: input file names; list of str
            output_filename: output pdf file name; str
            is_verbose: if print verbose information; bool, defaults to False
    """
    merger = PyPDF2.PdfFileMerger()
    print('combining ', end='')
    try:
        for f in filenames:
            print(f, end=' ')
            merger.append(fileobj=open(f, 'rb'))
    except IOError:
        print('cannot read file', f)
        raise
    print('... done')
    if not output_filename:
        i = 1
        while True:
            output_filename = 'binder{0:d}.pdf'.format(i)
            if os.path.isfile(output_filename):
                i += 1
            else:
                break
    elif os.path.isfile(output_filename):
        answer = input(output_filename + ' exists, overwrite?(y/n) ')
        if answer.lower() == 'y' or answer.lower == 'yes':
            pass
        else:
            raise FileExistsError(output_filename)
    print('writing', output_filename, '... ', end='')
    fileout = open(output_filename, 'wb')
    merger.write(fileout)
    fileout.close()
    print('done')

def pdf_extract(filename, output_filename, is_verbose=False):
    """
        extract from pdf files
        arguments:
            filename: input file name; str
            output_filename: output pdf file name; str
            is_verbose: if print verbose information; bool, defaults to False
    """
    print('reading pdf file', filename, ' ... ', end='')
    try:
       pdfin = PyPDF2.PdfFileReader(open(filename, 'rb')) 
    except IOError:
        print('cannot read file', filename)
        raise
    print('done')
    if not output_filename:
        i = 1
        while True:
            output_filename = 'excerpt{0:d}.pdf'.format(i)
            if os.path.isfile(output_filename):
                i += 1
            else:
                break
    elif os.path.isfile(output_filename):
        answer = input(output_filename + ' exists, overwrite?(y/n) ')
        if answer.lower() == 'y' or answer.lower == 'yes':
            pass
        else:
            raise FileExistsError(output_filename)
    while True:
        answer = input('choose which page/pages to be extracted (1-{0:d}) '.format(pdfin.getNumPages()))
        m = re.match('([0-9]+)-([0-9]+)', answer.strip())
        if m:
            if len(m.group(1)) + len(m.group(2)) + 1 == len(answer.strip()):
                page_start = int(m.group(1))
                page_end = int(m.group(2))
                if page_end < pdfin.getNumPages() + 1 and page_start <= page_end and page_start >= 1:
                    break
    pdfout = PyPDF2.PdfFileWriter()
    print('extracting page ', end='')
    for i in range(page_start - 1, page_end):
        print(i + 1, end=' ')
        pdfout.addPage(pdfin.getPage(i))
    print('... done')
    print('writing', output_filename, '... ', end='')
    fileout = open(output_filename, 'wb')
    pdfout.write(fileout)
    fileout.close()
    print('done')

if __name__ == '__main__':
    main()
    
